#!/usr/bin/env python

import json
from pathlib import Path

from cloudant.client import CouchDB
from requests import HTTPError


def main():
    try:
        user, auth_token = (
            (Path.home() / "private" / "couchdb-admin").read_bytes().splitlines()
        )
    except Exception:
        exit("failed to get CouchDB credentials")

    server = CouchDB(
        url="https://lhcb-couchdb.cern.ch/",
        user=user,
        auth_token=auth_token,
    )

    server.connect()
    db = server["nightlies-nightly"]
    param_files = []

    if "ci-tests" in db:
        doc = db["ci-tests"]
        while doc["requests"]:
            try:
                # get an object out of the queue
                request = doc["requests"].pop()
                doc.save()  # notify that the request is taken

                # create the parameters file for gitlab-mr job
                param_file = "params.{}.txt".format(len(param_files))
                with open(param_file, "w") as params:
                    params.write(
                        "content={}\n".format(json.dumps(request).replace("\\", "\\\\"))
                    )
                param_files.append(param_file)

            except HTTPError as err:
                if "conflict" not in str(err).lower():
                    raise
                # we didn't manage to "pop" a request from the document
                # due to concurrent access, let's update and try again
                doc.fetch()

    print("Triggering {} builds:".format(len(param_files)))
    for f in param_files:
        print("====", f, "====")
        print(open(f).read())


if __name__ == "__main__":
    main()

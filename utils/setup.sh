# Prepare a Rye based local installation to run commands

export RYE_HOME=${PWD}/.rye
export PATH=${RYE_HOME}/shims:${PATH}

# ensure rye
if ! which rye >&/dev/null ; then
    curl -sSf https://rye.astral.sh/get | RYE_INSTALL_OPTION="--yes --no-modify-path" bash
    (
        echo '[[sources]]'
        echo 'name = "default"'
        echo 'url = "https://lhcb-repository.web.cern.ch/repository/pypi/simple"'
    ) >> $(rye config --show-path)
fi

# install
rye sync --no-lock --no-dev || rye sync --no-lock --no-dev --force

export SSL_CERT_FILE=$(rye run python -m certifi)
$(dirname $0)/patch_certfile ${SSL_CERT_FILE}

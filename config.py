import os
from functools import reduce
from pathlib import Path

from lb.nightly.configuration import service_config

try:
    _config = service_config()
except Exception:
    _config = {}


def get_config(path, default=None):
    """
    Get config value for path (e.g. "couchdb.url")

    >>> _config = {"couchdb": {"url": "http://localhost:5984"}}
    >>> get_config("couchdb.url")
    'http://localhost:5984'
    """
    return (
        reduce(lambda d, k: d.get(k) if d else None, path.split("."), _config)
        or default
    )


COUCHDB_URL = os.environ.get("COUCHDB_URL") or get_config("couchdb.url")
ARTIFACTS_BASE_URL = (
    os.environ.get("ARTIFACTS_BASE_URL", "") or get_config("artifacts.uri", "")
).rstrip("/")
ARTIFACTS_INTERNAL_URL = os.environ.get("ARTIFACTS_INTERNAL_URL", ARTIFACTS_BASE_URL)
LOGS_BASE_URL = (
    os.environ.get("LOGS_BASE_URL", "") or get_config("logs.uri", "")
).rstrip("/")
LOGS_INTERNAL_URL = os.environ.get("LOGS_INTERNAL_URL", LOGS_BASE_URL)
# WORKER_LOGS_URL variable should look like e.g.:
# https://es-lhcb-core.cern.ch/kibana/app/discover#/view/4001ba70-898a-11ec-8af3-835be76c8b5a?_g=(filters:!(),refreshInterval:(pause:!f,value:8000),time:(from:now-24h,to:now))&_a=(columns:!(log,project,platform,command,task,host),filters:!(('$state':(store:appState),meta:(alias:!n,disabled:!f,index:'2c31c000-836b-11ec-8af3-835be76c8b5a',key:worker_task_id,negate:!f,params:(query:e0365bfb-982d-421f-9b06-492b2086d4f9),type:phrase),query:(match_phrase:(worker_task_id:{worker_task_id})))),index:'2c31c000-836b-11ec-8af3-835be76c8b5a',interval:auto,query:(language:kuery,query:''),sort:!())
WORKER_LOGS_URL = os.environ.get("WORKER_LOGS_URL", "")
GITLAB_URL = "https://gitlab.cern.ch/"
GITLAB_TOKEN = os.environ.get("GITLAB_TOKEN") or get_config("gitlab_token")
SECRET_KEY = os.environ.get("SECRET_KEY") or get_config("secret_key")
CERN_OAUTH_CLIENT_ID = os.environ.get("CERN_OAUTH_CLIENT_ID") or get_config(
    "cern_oauth.id"
)
CERN_OAUTH_CLIENT_SECRET = os.environ.get("CERN_OAUTH_CLIENT_SECRET") or get_config(
    "cern_oauth.secret"
)
WEBHOOK_KEY = os.environ.get("WEBHOOK_KEY") or get_config("webhook_key")

SLOTS_CONFIG_REPOSITORY = "https://gitlab.cern.ch/lhcb-core/LHCbNightlyConf.git"
SLOTS_LIST_URL = f"{SLOTS_CONFIG_REPOSITORY.replace('.git', '')}/-/jobs/artifacts/master/raw/slots-list.json?job=check-syntax"

CACHE_TYPE = "filesystem"
CACHE_DEFAULT_TIMEOUT = 300
CACHE_DIR = Path(__file__).parent / "cache"

LEGACY_COUCHDB_URL = (
    os.environ.get("LEGACY_COUCHDB_URL", "") or get_config("legacy.couchdb", "")
).rstrip("/")
LEGACY_BASE_URL = (
    os.environ.get("LEGACY_BASE_URL", "") or get_config("legacy.artifacts", "")
).rstrip("/")
LEGACY_INTERNAL_URL = os.environ.get("LEGACY_INTERNAL_URL", LEGACY_BASE_URL)
if "RMQPWD" not in os.environ:
    os.environ["RMQPWD"] = get_config("legacy.rabbitmq", "")

DOCS_PATH = os.environ.get("DOCS_PATH") or (
    Path(__file__).parent / "docs" / "_build" / "html"
)

MAX_TEST_XML_SIZE = int(os.environ.get("MAX_TEST_XML_SIZE_MB", 100)) * 1024**2
MAX_STDOUT_LENGTH = int(os.environ.get("MAX_STDOUT_SIZE_MB", 1)) * 1024**2

JENKINS_URL = os.environ.get("JENKINS_URL")
JENKINS_TOKENS = {
    "scheduler": os.environ.get("JENKINS_TOKEN_SCHEDULER"),
    "start-slots": os.environ.get("JENKINS_TOKEN_START_SLOTS"),
}

LEGACY_CI_TESTS = os.environ.get("LEGACY_CI_TESTS", "false").lower() in (
    "true",
    "1",
    "yes",
)

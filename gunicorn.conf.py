import os

# reload = True
timeout = 300
bind = "0.0.0.0:8080"
workers = int(os.environ.get("WEB_CONCURRENCY") or 4)

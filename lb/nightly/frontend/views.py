import json
import logging
import os
import re
from datetime import date, timedelta

import requests
from flask import (
    Response,
    redirect,
    render_template,
    request,
    send_from_directory,
    url_for,
)
from werkzeug.exceptions import HTTPException, NotFound

from lb.nightly.configuration.slot import Slot

from .api import get_slots_list
from .application import app, require_roles
from .errors import FileTooBig
from .models import (
    get_day_slots_summaries,
    get_latest_slots_summaries,
    get_slot_info,
    get_slot_page,
    get_slot_summary,
    latest_slot_build_ids,
)
from .models import legacy_db as _legacy_db
from .slots_config import get_slots, resolveMRs
from .utils import parse_id

FLAVOURS = set(["nightly", "testing", "release"])


@app.errorhandler(404)
def page_not_found(_error):
    return render_template("404.html"), 404


@app.errorhandler(Exception)
def handle_exception(error):
    if isinstance(error, FileTooBig):
        return render_template("500.html", error=error), 500

    # pass through everything else
    if isinstance(error, HTTPException):
        return error

    raise error


@app.route("/favicon.ico")
def favicon():
    return redirect(url_for("static", filename="images/lhcb_logo_icon.png"))


@app.route("/")
def index():
    return redirect(url_for("main", flavour="nightly"))


@app.route("/docs/")
@app.route("/docs/<path:filename>")
def docs(filename="index.html"):
    return send_from_directory(app.config["DOCS_PATH"], filename)


@app.route("/<flavour>/")
@app.route("/<flavour>/<day>/")
@app.route("/<flavour>/<slot>/<int:build_id>/")
def main(flavour, day="latest", slot=None, build_id=None):
    if flavour not in FLAVOURS:
        raise NotFound(f"bad flavour {flavour}")
    if not re.match(r"^(latest|\d{4}-\d{2}-\d{2})$", day):
        # if it's not a date, assume it's a slot name
        return slot_builds(flavour, slot=day)

    today = date.today()

    # FIXME: if invoked without day or slot we ignore flavour and use the "slots" query argument
    #        to display only the selected slots
    if day == "latest" and "slots" in request.args:
        slots = [
            get_slot_summary(*parse_id(slot.strip()))
            for slot in request.args["slots"].split()
        ]
        day = None  # no need to display a day if showing a specific set of slots
    else:
        slots = (
            list(
                get_latest_slots_summaries(flavour, since=today - timedelta(days=7))
                if day == "latest"
                else get_day_slots_summaries(flavour, day)
            )
            if slot is None
            else [get_slot_summary(flavour, slot, build_id)]
        )

    for slot_info in slots:
        slot_info["other_builds"] = latest_slot_build_ids(
            flavour, slot_info["name"], slot_info["build_id"] - 9
        )
        slot_info["other_builds"].reverse()

    return render_template(
        "slots_summary.html",
        flavour=flavour,
        week_dates=enumerate(today - timedelta(days=i) for i in range(7)),
        is_auth=None,
        day=day if slot is None else None,
        slots=slots,
        slot=slots[0] if len(slots) == 1 else None,
    )


def slot_builds(flavour, slot):
    """
    Render a view with the builds a given slot.
    """
    page = int(request.args.get("page", 0))
    if page < 0:
        raise NotFound(f"bad page number {page}")

    legacy = request.args.get("legacy", "true") == "true"

    slots = list(get_slot_page(flavour, slot, page, legacy))
    return render_template(
        "slots_summary.html",
        flavour=flavour,
        week_dates=None,
        is_auth=None,
        page=page,
        slots=slots,
        legacy=legacy,
    )


@app.route("/releases/")
def releases():
    return slot_builds("release", "lhcb-release")


@app.route("/<flavour>/<slot>/<int:build_id>/artifacts/")
def browse_artifacts(flavour, slot, build_id):
    data = get_slot_summary(flavour, slot, build_id)
    return render_template(
        "artifacts.html",
        title=f"Artifacts for {data['id']}",
        flavour=flavour,
        slot=data,
    )


@app.route("/download")
def download():
    """
    Helper to download artifacts giving them sensible names on the fly.

    For example:
      <base_url>/download?f=checkout/Gaudi/8e/8e1c61...0e8e.zip&n=Gaudi
    will download the file as "Gaudi.zip"
    """
    filename = request.args["f"]
    outputname = request.args["n"]
    root, ext = os.path.splitext(filename)
    _, second_ext = os.path.splitext(root)
    if second_ext == ".tar":
        ext = second_ext + ext
    base = app.config["ARTIFACTS_BASE_URL"]
    if "legacy" in request.args:
        base = app.config["LEGACY_BASE_URL"]
    elif "-report.json" in filename:
        base = app.config["LOGS_BASE_URL"]

    r = requests.get(f"{base}/{filename}", stream=True)
    return Response(
        r.iter_content(chunk_size=10 * 1024),
        content_type=r.headers["Content-Type"],
        headers={"Content-Disposition": 'filename="{}{}"'.format(outputname, ext)},
    )


@app.route("/start_slots", methods=["GET", "POST"])
@app.route("/<flavour>/start_slots", methods=["GET", "POST"])
@require_roles("builder")
def start_slots_form(flavour="nightly"):
    if request.method == "GET":
        slot = request.args.get("slot")

        try:
            build_id = request.args["build_id"]
            config = get_slot_info(flavour, slot, build_id)["config"]

            if "build_id" in config:
                del config["build_id"]
            config.setdefault("metadata", {})["config_id"] = "custom"

            rebuild = {
                "flavour": request.args.get("flavour", flavour),
                "slot": slot,
                "build_id": build_id,
                "config": json.dumps(config, indent=2),
            }

        except KeyError:
            rebuild = None

        return render_template(
            "start_slots.html",
            flavour=flavour,
            title=f"Start slots in {flavour}",
            slots_config=get_slots_list(),
            requested_slot=slot,
            rebuild=rebuild,
        )

    else:
        flavour = request.form.get("flavour", "nightly")

        if request.form.get("use_custom_slot"):
            slot = Slot.fromDict(json.loads(request.form.get("custom_slot")))
            slots = {slot.name: slot}
        else:
            commit = request.form.get("commit", "master")
            slots = request.form.getlist("slots")
            slots = get_slots(slots, commit)

        use_new_system = bool(request.form.get("use_new_system"))

        # The created Slot instances do not have a defined "flavour" yet
        for slot in slots.values():
            slot.flavour = flavour

        resolveMRs(slot for slot in slots.values() if slot)

        try:
            legacy_db = (
                _legacy_db(flavour) if app.config.get("LEGACY_COUCHDB_URL") else None
            )

            def add(slot):
                """
                Add a slot to the right DB.
                """
                from cloudant.error import CloudantDatabaseException

                from lb.nightly.db.database import Database

                # get the latest build id for the slot in both DBs, take the max and increase by one
                rows = (
                    list(
                        legacy_db.get_view_result(
                            "summaries",
                            "lastBuildId",
                            key=slot.name,
                            group=True,
                        )
                    )
                    if legacy_db is not None
                    else None
                )
                slot.build_id = (rows[0]["value"] + 1) if rows else 1

                if not use_new_system:
                    # flag the slot as "to be built" (legacy Jenkins)
                    slot.metadata["to_build"] = True

                while True:
                    # Note: we use some of the utils from lb.nightly.db,
                    #       but they require some adaptation
                    try:
                        doc = Database._slot2doc(slot)
                        doc["_id"] = "{}.{}".format(slot.name, slot.build_id)
                        legacy_db.create_document(doc, throw_on_exists=True)
                        break
                    except CloudantDatabaseException as err:
                        if "exists" not in str(err):
                            raise
                        slot.build_id += 1

                if use_new_system:
                    task = f"{slot.flavour}/{slot.name}/{slot.build_id}"
                    response = requests.post(
                        f"{app.config['JENKINS_URL']}/buildByToken/buildWithParameters?job=scheduler",
                        data={
                            "token": app.config["JENKINS_TOKENS"]["scheduler"],
                            "task": task,
                        },
                        verify=False,  # FIXME: remove this
                    )
                    if not response.ok:
                        logging.warning(
                            "failed to schedule build of %s: %s", task, response.text
                        )
                        return None

                return slot

            # Add the new slot instances to CouchDB
            added_slots = [add(slot) for slot in slots.values() if slot]

            if len(added_slots) == 1:
                slot = added_slots[0]
                return redirect(
                    url_for(
                        "main",
                        flavour=slot.flavour,
                        slot=slot.name,
                        build_id=slot.build_id,
                    )
                )
            else:
                return redirect(
                    url_for(
                        "main",
                        flavour="nightly",
                        slots=" ".join([slot.id() for slot in added_slots]),
                    )
                )
        except requests.HTTPError as err:
            logging.warning("%s", err)
            if "forbidden" in str(err).lower():
                # too bad, we do not have the right credentials, let's just print the slot configs
                return {
                    slot: slots[slot].toDict() if slots[slot] else None
                    for slot in slots
                }
            raise

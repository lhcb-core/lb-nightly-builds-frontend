import logging
from datetime import date, timedelta

import requests
from flask import flash, redirect, render_template, request, url_for
from requests import HTTPError
from werkzeug.exceptions import BadRequest

from .application import app, require_roles
from .models import get_periodic_tests, get_slots_since_day, legacy_db

_CONFIGURATIONS_DOC_NAME = "frontend:periodic-configuration"
_TEST_REQUESTS_DOC_NAME = "frontend:tests-to-start"


def _db():
    return legacy_db("periodic")


@app.route("/periodic/")
def periodic():
    n_of_entries = int(request.args.get("n", 2000))
    entries = get_periodic_tests(n_of_entries)
    return render_template(
        "periodic_tests.html",
        title="LHCb Periodic Tests",
        entries=entries,
    )


@app.route("/periodic/start", methods=["GET"])
@require_roles("builder")
def start_periodic_test():
    # get slot build info for the last 7 days
    builds_info = {}
    for entry in get_slots_since_day("nightly", since=date.today() - timedelta(days=7)):
        slot = builds_info.setdefault(entry["slot"], {})
        slot[entry["build_id"]] = {
            "projects": entry["projects"],
            "platforms": entry["platforms"],
        }

    return render_template(
        "start_periodic_test.html",
        title="Launch test",
        builds_info=builds_info,
    )


@app.route("/periodic/start", methods=["POST"])
@require_roles("builder")
def start_periodic_test_trigger():
    try:
        configs = _db()[_CONFIGURATIONS_DOC_NAME]["configurations"]
        fields = dict(request.form)

        for pk in [int(key) for key in request.form.getlist("group_env")]:
            config = [entry for entry in configs if pk == entry["pk"]][0]
            fields.update(config)

            task = f"nightly/{fields['slot']}/{fields['build_id']}"
            exec_name, handlers = fields["env"].split("|", 1)
            response = requests.post(
                f"{app.config['JENKINS_URL']}/buildByToken/buildWithParameters?job=lhcb-pr-test",
                data={
                    "token": app.config["JENKINS_TOKENS"]["scheduler"],
                    "task": task,
                    "project": f"{fields['project']}/{fields['platform']}",
                    "node": fields["os_label"],
                    "name": fields["group"],
                    "exec_name": exec_name,
                    "handlers": handlers,
                    "runs_count": 1,
                },
                verify=False,  # FIXME: remove this
            )
            if response.ok:
                flash(
                    "'{group} : {env}' for {project} {platform} in {slot}/{build_id}".format(
                        **fields
                    ),
                    "started_tests",
                )
            else:
                logging.warning(
                    "failed to schedule lhcb-pr-test for %s/%s/%s",
                    task,
                    fields["project"],
                    fields["platform"],
                )
                flash(
                    "'{group} : {env}' for {project} {platform} in {slot}/{build_id}".format(
                        **fields
                    ),
                    "started_tests_failures",
                )

    except KeyError:
        raise BadRequest()

    # FIXME: it would be nice to have some sort of confirmation of triggering
    return redirect(url_for("periodic"))


@app.route("/periodic/configs", methods=["GET", "POST"])
@require_roles("builder")
def periodic_configs():
    if request.method == "POST":
        if request.form["action"] in ("remove", "add"):
            doc = _db().create_document(
                {
                    "_id": _CONFIGURATIONS_DOC_NAME,
                    "type": "periodic-configuration",
                    "configurations": [],
                }
            )
            if "_rev" not in doc:
                # document already in the database, fetch the most recent version
                doc.fetch()

            to_remove = set(int(k) for k in request.form.getlist("keys[]"))
            pk = -1

            # we give ourselves 5 attempts to resolve conflicts (we never need more than one)
            for _attempt in range(5):
                try:
                    if request.form["action"] == "remove":
                        # keep all entries except those to remove
                        doc["configurations"] = [
                            entry
                            for entry in doc["configurations"]
                            if entry["pk"] not in to_remove
                        ]
                    else:
                        group = request.form.get("group")
                        environment = request.form.get("environment")
                        # find the highest "primary key" value
                        # (and check if we actually need to add the entry)
                        for entry in doc["configurations"]:
                            if (entry["group"], entry["env"]) == (
                                group,
                                environment,
                            ):
                                # it's already there, no need to add it
                                return {
                                    "configurations": doc["configurations"],
                                    "status": "failure",
                                    "select": entry["pk"],
                                }
                            else:
                                pk = max(pk, entry["pk"])
                        # let's add the entry with a valid "pk"
                        pk += 1
                        doc["configurations"].append(
                            {"group": group, "env": environment, "pk": pk}
                        )
                        # and keep the list sorted
                        doc["configurations"].sort(key=lambda e: (e["group"], e["env"]))

                    doc.save()
                    break
                except HTTPError as err:
                    if "conflict" not in str(err).lower():
                        raise
                    doc.fetch()
            else:
                doc.fetch()
                return {
                    "configurations": doc["configurations"],
                    "status": "failure",
                    "select": pk,
                }

            return {
                "configurations": doc["configurations"],
                "status": "success",
                "select": pk,
            }

        else:
            # neither add nor remove, let's pretend it's a GET request
            pass

    if _CONFIGURATIONS_DOC_NAME in _db():
        return {
            "configurations": _db()[_CONFIGURATIONS_DOC_NAME].get("configurations", [])
        }
    return {"configurations": []}

var filters = []

function addNewFilter(){
    $('#filters_active_filters').hide();
    $('#filters_add_new_filter').show();
    $('#add_new_filter_button').hide();
    $('#filter_modal_cancel').show();

    addNewFilterNextStep(1, "Select interested days")
}

function addNewFilterNextStep(step_number, title){
    $('.filters_add_new_filter_step').hide();
    $('#filters_add_new_filter_step_'+step_number).show();
    $('#filters_modal_label').html(title);
    $('#filter_modal_button').html("Next step: "+ (step_number+1))
}

function nextButton(){
    var button_text = $('#filter_modal_button').html();
    if(button_text == "Close"){
        location.reload();
    }
    else if (button_text == "Save"){
        var interest = {
            'days': [],
            'slots': [],
            'projects': []
        }
        $('.day_checkbox').each(function( index ){
            if($(this).prop('checked')){
                interest['days'].push($(this).val())
            }
        });
        $('.day_of_week_checkbox').each(function( index ){
            if($(this).prop('checked')){
                interest['days'].push($(this).val())
            }
        });
        $('.filter_slots').each(function( index ){
            if($(this).prop('checked')){
                interest['slots'].push($(this).val())
            }
        });
        $('.filter_projects').each(function( index ){
            if($(this).prop('checked')){
                interest['projects'].push($(this).val())
            }
        });
        if(filters == undefined){
            filters = [];
        }
        filters.push(interest);
        $.cookie("filters_full", JSON.stringify(filters));
        refreshModalFilters();
    }
    else{
        var step = button_text.split("Next step: ")[1]
        if(step == "2"){
            generateSlots()
        }
        if(step == "3"){
            generateProjects()
        }
    }
}

function buildActiveFilters(){
    var html = "";
    for(var i = 0; i < filters.length; i++){
        html += '<div class="alert alert-success alert-dismissible" role="alert">';
        html += '<button type="button" onclick="deleteFilter('+i+')" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
        var filter = filters[i];
        html += "<strong>Filter</strong> ";
        if(filter['days'][0] == "yesterday" || filter['days'][0] == "today"){
            for(var j = 0; j < filter['days'].length; j++){
                html += filter['days'][j] +', ';
            }
        }
        else{
            for(var j = 0; j < filter['days'].length; j++){
                html += days[filter['days'][j]] +', ';
            }
        }
        html = html.slice(0, html.length - 2);
        html += "<strong> for slots </strong> ";
        for(var j = 0; j < filter['slots'].length; j++){
            html += filter['slots'][j] +', ';
        }
        html = html.slice(0, html.length - 2);
        html += " <strong> with projects </strong>";
        for(var j = 0; j < filter['projects'].length; j++){
            html += filter['projects'][j] +', ';
        }
        html = html.slice(0, html.length - 2);
        html += '</div>'
    }
    if(html == ""){
        html += '<div class="alert alert-warning alert-dismissible" role="alert">';
        html += 'No filters applied!</div>'

    }
    $('#filters_active_filters').html(html);
}

function refreshModalFilters(){
    buildActiveFilters();
    $('#filters_active_filters').show();
    $('#filters_add_new_filter').hide();
    $('#add_new_filter_button').show();
    $('#filter_modal_cancel').hide();
    removeChecks("day_checkbox");
    removeChecks('day_of_week_checkbox');
    $('#filter_modal_button').html("Close");
}

function closeModal(){
    refreshModalFilters();
    $('#filters_modal').modal('hide');

}

function deleteFilter(index){
    filters.splice(index, 1);
    console.log(filters);
    if (filters.length == 0){
        $.removeCookie("filters_full");
    }
    else{
       $.cookie("filters_full", JSON.stringify(filters));
    }
}

function generateProjects(){
    col = ["",""];
    var intresetead_slots = [];
    var intresetead_projects = [];
    $('.filter_slots').each(function( index ){
        if($(this).prop('checked')){
            intresetead_slots.push($(this).val())
        }
    });

    for(slot in intresetead_slots){
        var slot_projects = slots_names[intresetead_slots[slot]];
        for(index in slot_projects){
            var project = slot_projects[index]
            if(intresetead_projects.indexOf(project) < 0)
                intresetead_projects.push(project)
        }
    }
    intresetead_projects.sort();
    var i = 0;
    for(index in intresetead_projects){
        var project = intresetead_projects[index]
        html = '<div class="checkbox"><label><input class="filter_projects" onclick="removeCheckAll(\'filter_projects\')" type="checkbox"';
        html += ' value="' + project +'">' + project + '</label></div>';
        col[i%2] += html
        i++;
    }
    $('#filter_modal_projects_col_1').html(col[0]);
    $('#filter_modal_projects_col_2').html(col[1]);
    addNewFilterNextStep(3, "Select interested projects")
    $('#filter_modal_button').html("Save");
}


function generateSlots(){
    col = ["",""];
    var i = 0;
    for(slot_name in slots_names){
        html = '<div class="checkbox"><label><input class="filter_slots" onclick="removeCheckAll(\'filter_slots\')" type="checkbox"';
        html += ' value="' + slot_name +'">' + slot_name + '</label></div>';
        col[i%2] += html
        i++;
    }
    $('#filter_modal_slots_col_1').html(col[0]);
    $('#filter_modal_slots_col_2').html(col[1]);
    addNewFilterNextStep(2, "Select interested slots")
}

function removeChecks(cls){
    cls_arraies = [['day_checkbox','day_of_week_checkbox']];
    for(index in cls_arraies){
        var cls_array = cls_arraies[index]
        if(cls_array.indexOf(cls) >= 0){
            for(index in cls_array){
                var cls_tmp = cls_array[index];
                if(cls_tmp != cls){
                    $('.'+ cls_tmp).prop('checked',false);
                    $("#all_"+cls_tmp).prop('checked', false);
                }
            }
            break;
        }
    }
}

function checkAll(cls){
    $('.'+ cls).prop('checked',$("#all_"+cls).prop('checked'));
    removeChecks(cls);
}

function removeCheckAll(cls){
    var was_removed = false;
    $('.'+ cls).each(function( index ){
        if(!$(this).prop('checked')){
            $("#all_"+cls).prop('checked', false);
            was_removed = true;
        }
    });
    removeChecks(cls);

    if(!was_removed)
        $("#all_"+cls).prop('checked', true);
}

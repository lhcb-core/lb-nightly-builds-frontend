import re
from datetime import date, datetime
from itertools import chain

import dateutil.tz
import requests
from markupsafe import Markup, escape

from .application import app, cache
from .utils import test_anchor

GENEVA_TZ = dateutil.tz.gettz("Europe/Zurich")


@app.template_filter("fuzzy_date")
def fuzzy_date(value):
    """Convert a date string to a fuzzy time"""
    if not value:
        return ""

    value_date = date.fromisoformat(value)
    days_ago = (datetime.now(tz=GENEVA_TZ).date() - value_date).days
    if days_ago == 0:
        fuzzy = "today"
    elif days_ago == 1:
        fuzzy = "yesterday"
    elif days_ago <= 3:
        fuzzy = f"{days_ago} days ago"
    else:
        fuzzy = None
    return Markup(f'<span title="{value}">{fuzzy}</span>') if fuzzy else value


@cache.memoize(timeout=3600)
def gitlab_mr_title(project, mr):
    """
    Retrieve from gitlab the title of a merge request.

    See https://docs.gitlab.com/ee/api/merge_requests.html#get-single-mr
    """
    return (
        requests.get(
            f"https://gitlab.cern.ch/api/v4/projects/{project.replace('/', '%2F')}/merge_requests/{mr}"
        )
        .json()
        .get("title")
    )


def gitlab_mr_link(matchobj):
    """
    Helper to format a merge request link via a ``re.sub()``.
    """
    project, mr = matchobj.group(1), int(matchobj.group(2))
    title = escape(gitlab_mr_title(project, mr))
    return (
        f'<a href="https://gitlab.cern.ch/{project}/-/merge_requests/{mr}" target="_blank" class="merge-request"'
        + (f' data-toggle="tooltip" title="{title}"' if title else "")
        + f">{project}!{mr}</a>"
    )


@app.template_filter("gitlab_links")
def gitlab_links(text, gitlab_id):
    """
    Convert special patterns in text to links to gitlab.
    """
    text = escape(text)

    # replace group/Project!123 with link to merge request
    text = re.sub(
        r"([a-zA-Z0-9-_.]+/[a-zA-Z0-9-_./]+)!(\d+)",
        gitlab_mr_link,
        text,
    )

    # we cannot link commit ids witout a gitlab_id
    if gitlab_id:
        # replace commit ids with links to gitlab
        text = re.sub(
            r"(?<!mmit: |tree: |.....@)\b([a-f0-9]{7,40})\b",
            f'<a href="https://gitlab.cern.ch/{gitlab_id}/-/commit/\\1" target="_blank">\\1</a>',
            text,
        )

    return Markup(text)


@app.template_filter("highlight_path_fragments")
def highlight_path_fragments(text):
    if ":" in text and "://" not in text:
        text = Markup(
            ":".join(
                f'<span class="path-fragment">{fragment}</span>'
                for fragment in text.split(":")
            )
        )
    return text


@app.template_filter("issue_locations")
def issue_locations(issues):
    """
    Helper to format the build issues summary for easier handling in the template.

    >>> issue_locations([
    ...         {"desc": "issue 1",
    ...          "where": {"Dir1": ["case1", "case2"],
    ...                    "Dir2": ["case3"]}},
    ...         {"desc": "issue 2",
    ...          "where": {"Dir3": ["case1"]}},
    ...         ]
    ... )
    [('issue 1', [('Dir1', 'case1', 'Dir1 #1'), ('Dir1', 'case2', 'Dir1 #2'), ('Dir2', 'case3', 'Dir2')]), ('issue 2', [('Dir3', 'case1', 'Dir3')])]
    """
    return [
        (
            issue["desc"],
            [
                (
                    section,
                    anchor,
                    f"{section} #{idx}"
                    if len(issue["where"][section]) > 1
                    else section,
                )
                for section in issue["where"]
                for idx, anchor in enumerate(issue["where"][section], 1)
            ],
        )
        for issue in issues
    ]


@app.template_filter("count_issues")
def count_issues(issues):
    """
    Helper to count the total number of issues reported.

    >>> count_issues([
    ...         {"desc": "issue 1",
    ...          "where": {"Dir1": ["case1", "case2"],
    ...                    "Dir2": ["case3"]}},
    ...         {"desc": "issue 2",
    ...          "where": {"Dir3": ["case1"]}},
    ...         ]
    ... )
    4
    """
    return sum(
        sum(len(locations) for locations in issue["where"].values()) for issue in issues
    )


def build_hierarchy(key, value):
    # start the row for the group
    ret = [f"<tr><th>{key}</th><td>"]

    if not value:
        ret.append("<i>No data available</i></td></tr>")
        return "".join(ret)

    current_group = None
    current_table = {}

    # map pytest outcomes to bootstrap severity levels
    SEVERITY_LEVELS = {"passed": "success", "skipped": "warning", "failed": "danger"}

    # if the key is a top level key, we show the elements as a table, ex:
    # - key
    #   - | property1 | value1 |
    #     |-----------|--------|
    #     | property2 | value2 |

    if "." not in list(value.keys())[0]:
        ret.append('<table class="list-group-item big">')
        for k, v in value.items():
            ret.append(f"<tr><th>{k}</th><td>{v}</td></tr>")
        ret.append("</table>")

    # otherwise we group by the first part of the key and show the elements as a list
    # each list item is a table, ex:
    # - key
    #   - value1
    #         | subkey1 | subval1 |
    #         |---------|---------|
    #         | subkey2 | subval2 |
    #   - value2
    #         | subkey1 | subval1 |

    else:
        ret.append('<ul class="list-group tests-results" id="results">')
        for k, v in value.items():
            split_keys = re.split(r"\.(?![^\[]*\])", k)
            if split_keys[0] != current_group:
                if current_group is not None:
                    severity = SEVERITY_LEVELS.get(
                        current_table.get("outcome", "default"), "default"
                    )
                    ret.append(render_group(current_group, current_table, severity))
                    current_table = {}
                current_group = split_keys[0]
            current_table.update({".".join(split_keys[1:]): v})
        if current_group:
            severity = SEVERITY_LEVELS.get(
                current_table.get("outcome", "default"), "default"
            )
            ret.append(render_group(current_group, current_table, severity))
        ret += "</ul>"
    ret += "</td></tr>"
    return "".join(ret)


def render_group(group_name, group_content, severity):
    ret = []
    if severity == "success":
        icon = "glyphicon glyphicon-ok-circle"
        text = ""
    elif severity == "danger":
        icon = "glyphicon glyphicon-remove-circle"
        text = " (See Pytest Output)"
    elif severity == "warning":
        icon = "glyphicon glyphicon-ban-circle"
        text = " (skipped)"
    else:
        icon = ""
        text = ""

    ret.append(
        f'<li class="list-group-item list-group-item-{severity} collapsed leaf">'
    )

    # we don't create a table if the only key is "outcome" nor for skipped tests
    if (
        not (len(group_content) == 1 and "outcome" in group_content)
        and severity != "warning"
    ):
        ret.append(f"""
            <span class="loaded-test-name" style="cursor: pointer;">
                <span class="collapse-icon glyphicon"></span>
                {group_name}</span>
        """)
        ret.append('<table class="list-group-item">')
        for k, v in group_content.items():
            if k != "outcome":
                ret.append(f'<tr><th style="width:175px">{k}</th><td>{v}</td></tr>')
        ret.append("</table>")

    else:
        ret.append(f"""
            <span class="loaded-test-name">
                <span class="{icon}"></span>
                {group_name} {text}</span>
        """)
    ret.append("</li>")
    return "".join(ret)


def format_test_body_pytest(node):
    ctest_keys = [
        "CTest Full Command Line",
        "Command Line",
        "Completion Status",
        "Environment",
        "Execution Time",
        "Processors",
        "Status",
        "Exit Code",
        "Exit Value",
    ]

    keys_to_ignore = [
        "test_record_options",
    ]

    # format and sort the properties
    processed_items = []
    for key, value in chain(
        (item for item in node.data.items() if item[0] != "results"),
        node.data.get("results", {}).items(),
    ):
        if key in ctest_keys:
            key = f"CTest Report.{key}"

        if "test_fixture_setup" in key:
            key = key.split(".")[-1]

        if "doc" in key:
            key = key.split(".")[0] + ".doc.doc"

        if key == "output":
            key = "pytest_output"

        # turn snake_case properties into Title Case
        if all(k not in key for k in keys_to_ignore):
            if not (key.startswith("Test") or key.startswith("CTest Report")):
                key = " ".join(word.capitalize() for word in key.split("_"))
            processed_items.append((key, value))

    keys_order = [
        "Command",
        "Expanded Command",
        "Reference File",
        "Options",
        "Cwd",
        "Runtime Environment",
        "Start Time",
        "End Time",
        "Elapsed Time",
        "Stdout",
        "Stderr",
        "Pytest Output",
    ]
    keys_order_index = {key: index for index, key in enumerate(keys_order)}

    def custom_sort(item):
        """
        Sort keys based on priority:
        (0) Labels
        (1) Test items
        (2) Keys in the predefined order (keys_order)
        (3) Other keys in alphabetical order
        (4) CTest Report keys
        """
        key = item[0]
        if key.startswith("Labels"):
            return (0, key.lower())
        elif key.startswith("Test"):
            return (1, key.lower())
        elif key in keys_order_index:
            return (2, keys_order_index[key])
        elif key.startswith("CTest Report"):
            return (4, key.lower())
        else:
            return (3, key.lower())

    processed_items.sort(key=custom_sort)

    # start the table body for the test results
    yield "<table><tbody>"

    # for "TestClass.test_name.property" we group by "TestClass"
    current_group = None
    group_content = {}

    for key, value in processed_items:
        content = value if isinstance(value, str) else str(value)

        # no need to print Outcome, but we need to keep it for setting the severity level
        if key.startswith("Outcome"):
            continue

        # format the labels as badges
        if key == "Labels":
            labels = content[1:-1].split(",")
            parsed_labels = [label.strip().strip("'\"") for label in labels]
            content = "  ".join(
                f'<span class="badge rounded-pill bg-secondary">{label}</span>'
                for label in parsed_labels
            )

        # format the environment as a table alphanumerically sorted
        if key == "Runtime Environment":
            content = '<table class="list-group-item big">'
            lines = [line for line in value.split("\n") if line]
            lines.sort()
            for line in lines:
                if "=" in line:
                    kk, vv = line.split("=", 1)
                    content += f"<tr><th>{kk}</th><td>{vv}</td></tr>"
            content += "</table>"

        # escape the code blocks
        if key in {"Pytest Output"} or ("\n" in content and not value.startswith("<")):
            content = f"<pre>{escape(content)}</pre>"

        # collapse the blocks with more than 7 lines
        if content.startswith("<pre>") and content.count("\n") > 7:
            content = '<pre class="big">' + content[5:]

        # check if we have a top level key
        if key.find(".") == -1:
            # close the previous hierarchy group if it hasn't been written yet and start a new one
            if current_group is not None:
                yield build_hierarchy(current_group, group_content)
                current_group = None
            # write the current key-value pair
            yield f'<tr><th style="width:175px">{key}</th><td>{content}</td></tr>'
        # otherwise we group by the first part of the key
        else:
            split_keys = key.split(".")
            # check if we have a new group
            if split_keys[0] != current_group:
                if current_group is not None:
                    yield build_hierarchy(current_group, group_content)
                current_group = split_keys[0]
                group_content = {}
            # store the content for the current group
            group_content[".".join(split_keys[1:])] = content

    # write the last group
    if current_group is not None:
        yield build_hierarchy(current_group, group_content)

    # end the table body for the test results
    yield "</tbody></table>"


def format_test_body_generic(node):
    yield "<table><tbody>"

    for key, value in chain(
        (item for item in node.data.items() if item[0] != "results"),
        node.data["results"].items(),
    ):
        content = value if isinstance(value, str) else str(value)
        if key == "output":
            content = f"<pre>{escape(content)}</pre>"
        elif "\n" in content and not value.startswith("<"):
            # probably a block we expect to be "<pre>"-formatted
            content = f"<pre>{escape(content)}</pre>"

        # the collapsed block shows up to 5-6 lines, so we do not collapse if we have
        # less than 8
        if content.startswith("<pre>") and content.count("\n") > 7:
            content = '<pre class="big">' + content[5:]
        yield f"<tr><th>{key}</th><td>{content}</td></tr>"

    yield "</tbody></table>"


@app.template_filter("make_treeview")
def make_treeview(tests, full=False):
    """
    Special filter to generate a tree view of a the tree of tests in TestResults.
    """
    from .utils import TestResults

    # results = [(test.find("Name").text, test.attrib["Status"]) for test in xml.findall("./Testing/Test")]
    CLASS_FOR_STATUS = dict(
        [
            ("passed", "success"),
            ("failed", "danger"),
            ("error", "danger"),
            ("skipped", "warning"),
            ("notrun", "warning"),
            ("untested", "warning"),
        ]
    )
    SEVERITY_LEVELS = ("success", "warning", "danger")

    tests = TestResults.from_dict({"tests": tests}).tests

    def get_branch_level(node):
        if node.data:
            status = node.data.get("Status", "passed")
            results = node.data.get("results", {})
            skipped_outcome = results.get("outcome.skipped", None)
            passed_outcome = results.get("outcome.passed", None)
            # ctest reports 'skipped' pytest tests as 'passed' so we check both
            if status == "passed" and skipped_outcome and not passed_outcome:
                level = SEVERITY_LEVELS.index("warning")
            else:
                level = SEVERITY_LEVELS.index(CLASS_FOR_STATUS[status])
        else:
            level = 0
        for child in node.children:
            level = max(level, get_branch_level(child))
        return level

    def format_node(node):
        # FIXME: this is a very inefficient way (as it computes the level several times for the leaves)
        severity = SEVERITY_LEVELS[get_branch_level(node)]
        test_id = node.id.replace("(root).", "")
        anchor = test_anchor(test_id)
        yield f'<li class="list-group-item list-group-item-{severity} collapsed'
        if node.data:
            yield " leaf"
        yield f'" id="{anchor}"'
        if severity == "success":  # we do not show (by default) the passed tests
            yield ' style="display: none;"'
        yield (
            f'><span class="test-name"><span class="collapse-icon glyphicon"></span> {node.name} '
            f'<a href="#{anchor}" title="permanent link" class="glyphicon glyphicon-link"></a></span>'
        )
        if node.children:
            yield '<ul class="list-group">'
            for fragment in chain.from_iterable(
                format_node(child) for child in node.children
            ):
                yield fragment
            yield "</ul>"
        elif node.data:
            if "results" in node.data and "Causes" in node.data["results"]:
                yield f' <span class="causes text-info">{node.data["results"]["Causes"]}</span>'
            elif "results" in node.data and "outcome.failed" in node.data["results"]:
                # format the causes of failures in pytest tests
                failed_tests = node.data["results"]["outcome.failed"]
                items = failed_tests[1:-1].split(",")
                parsed_tests = [test_name.strip().strip("'\"") for test_name in items]
                formatted_tests = ", ".join(
                    test_name.split(".")[1].replace("test_", "")
                    if "." in test_name
                    else test_name.replace("test_", "")
                    for test_name in parsed_tests
                )
                yield f' <span class="causes text-info">{formatted_tests}</span>'
            elif severity == "danger":
                yield ' <span class="causes text-info"><i>unknown</i></span>'

            yield (
                '<ul class="list-group data">'
                '<li class="list-group-item list-group-item-default">'
            )

            if full:
                if any("pytest" in label.lower() for label in node.data["Labels"]):
                    yield from format_test_body_pytest(node)
                else:
                    yield from format_test_body_generic(node)
            else:
                yield f'<span class="spinner" test_id="{test_id}">loading...</span>'

            yield "</li></ul>"

        yield "</li>"

    return Markup(
        f'<ul class="list-group tests-results" id="results">{"".join(chain.from_iterable(format_node(child) for child in tests.children))}</ul>'
    )


@app.context_processor
def gitlab_helpers():
    return dict(gitlab_mr_title=gitlab_mr_title)


@app.template_filter("report_exception")
def report_exception(exception_info, placement="bottom"):
    return Markup(
        '<span data-toggle="popover" title="exception:" '
        f'data-placement="{escape(placement)}" data-html="true" '
        f'data-content="&lt;p&gt;{escape(exception_info["desc"])}&lt;/p&gt;&lt;em&gt;'
        f'at {escape(exception_info["time"])}&lt;/em&gt;" '
        'data-trigger="hover"><span class="glyphicon glyphicon-warning-sign text-danger"></span> '
        "<em>error</em></span>"
    )
